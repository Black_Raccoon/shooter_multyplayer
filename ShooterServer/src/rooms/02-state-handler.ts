import { Room, Client } from "colyseus";
import { Schema, type, MapSchema } from "@colyseus/schema";

export class Player extends Schema {
   
    @type("uint8")
    skin = 0;

    @type("int8")
    gun = 0;
     
    @type("int8")
    loss = 0;
  
    @type("number")
    speed = 0;

    @type("int8")
    maxHp = 0;

    @type("int8")
    currentHp = 0;

    @type("number")
    pX = 0;

    @type("number")
    pY = 0;

    @type("number")
    pZ = 0;

    @type("number")
    vX = 0;

    @type("number")
    vY = 0;
    
    @type("number")
    vZ = 0;

    @type("number")
    rX = 0;
    
    @type("number")
    rY = 0;

    @type("number")
    avY = 0;

    @type("boolean")
    sD = false;
}

export class State extends Schema {
    @type({ map: Player })
    players = new MapSchema<Player>();

    something = "This attribute won't be sent to the client-side";

    createPlayer(sessionId: string, data: any, spawnPointIndex: number, skinIndex: number) {
        const player = new Player();
        player.speed = data.speed;
        player.maxHp = data.hp;
        player.currentHp = data.hp;
        player.skin = skinIndex;

       
       /* const countSpawnPoint = data.countSpPoint;
        const spawnPointIndex = Math.floor(Math.random() * countSpawnPoint) - 1;
        const xSpawnPoint = data.spawnPoint.Points[spawnPointIndex].x;
        const ySpawnPoint = data.spawnPoint.Points[spawnPointIndex].y;
        const zSpawnPoint = data.spawnPoint.Points[spawnPointIndex].z;
        
        player.pX = xSpawnPoint;
        player.pY = ySpawnPoint;
        player.pZ = zSpawnPoint;
        */
        
       // player.pX = data.pX;
       // player.pY = data.pY;
       // player.pZ = data.pZ;
       // player.rY = data.rY;

       player.pX = spawnPointIndex;
       this.players.set(sessionId, player);
    }

    removePlayer(sessionId: string) {
        this.players.delete(sessionId);
    }

    movePlayer (sessionId: string, data: any) {
            const player = this.players.get(sessionId);
            player.pX = data.pX;
            player.pY = data.pY;
            player.pZ = data.pZ;
            player.vX = data.vX;
            player.vY = data.vY;
            player.vZ = data.vZ;
            player.rX = data.rX;
            player.rY = data.rY;
            player.avY = data.avY;
            player.sD = data.sD;
    }

    changeGun (sessionId: string, data: any) {
        const player = this.players.get(sessionId);
        player.gun = data.index
    }   
}

export class StateHandlerRoom extends Room<State> {
    maxClients = 4;
    spawnPointCount = 1;
    skinsCount = 1;

    onCreate (options) {
        console.log("StateHandlerRoom created!", options);

        this.spawnPointCount = options.countPoint;
        
        this.skinsCount = options.skinsCount;
        
        
        this.setState(new State());

        this.onMessage("move", (client, data) => {
            this.state.movePlayer(client.sessionId, data);
        });

        this.onMessage("changeGun", (client, data) => {
            this.state.changeGun(client.sessionId, data);           
        });


        this.onMessage("shoot", (client, data) => {
            this.broadcast("SRV_shoot", data, {except: client});           
        });

        this.onMessage("damage", (client , data) =>{
            const damagedClientId = data.id; 
            const damagedPlayer = this.state.players.get(damagedClientId);
            let hp = data.currentHp
            if(hp>0){
                damagedPlayer.currentHp = hp;
                return;
            }
            damagedPlayer.loss += 1;
            damagedPlayer.currentHp = damagedPlayer.maxHp;
            
            for(var i = 0; i< this.clients.length; i++ ){
                if(this.clients[i].id == damagedClientId){
                   
                    //const x = Math.floor(Math.random() * 50) -25;
                    //const z = Math.floor(Math.random() * 50) -25;

                    //const message = JSON.stringify({x,z});

                    const spawnPointIndex = Math.floor(Math.random() * this.spawnPointCount);
                    this.clients[i].send("Restart", spawnPointIndex);
                }
            }       
        });
    }

    onAuth(client, options, req) {
        return true;
    }

    onJoin (client: Client, data: any) {
        if(this.clients.length > 1)
            this.lock;
       
        const spawnPointIndex = Math.floor(Math.random() * this.spawnPointCount);
        const skinIndex = Math.floor(Math.random() * this.skinsCount);
        this.state.createPlayer(client.sessionId, data, spawnPointIndex, skinIndex);
    }

    onLeave (client) {
        this.state.removePlayer(client.sessionId);
    }

    onDispose () {
        console.log("Dispose StateHandlerRoom");
    }

}
