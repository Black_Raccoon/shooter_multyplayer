using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    public class IntervalCalculator
    { 
        private readonly List<float> _onChangeIntervals = new() { 0, 0, 0, 0, 0 };
        private float _previousTime = 0;

        public float AverageInterval
        {
            get
            {
                float sumInterval = 0f;
                foreach (float interval in _onChangeIntervals) 
                    sumInterval += interval;
                return sumInterval / _onChangeIntervals.Count;
            }
        }

        public void SaveTime()
        {
            float interval = Time.time - _previousTime;
            _previousTime = Time.time;
            _onChangeIntervals.Add(interval);
            _onChangeIntervals.RemoveAt(0);
        }
    }
}