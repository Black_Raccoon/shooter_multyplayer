using System;
using UnityEngine;

namespace Common
{
    public class LossCounter : MonoBehaviour
    {
        private int _playerLoss = 0;
        private int _enemyLoss = 0;
        public event Action<int, int> OnCountChanged;

        public void SetPlayerLoss(int value)
        {
            _playerLoss = (int)value;
            OnCountChanged?.Invoke(_enemyLoss, _playerLoss);
        }

        public void SetEnemyLoss(int value)
        {
            _enemyLoss = (int)value;
            OnCountChanged?.Invoke(_enemyLoss, _playerLoss);
        }
    }
}