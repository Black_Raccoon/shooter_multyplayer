using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunRay : MonoBehaviour
{
    [SerializeField] private Transform _ray;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private Transform _point;
    [SerializeField] private float _pointSize;
    private Transform _camera;
    private Vector3 _sizeVector;
    private readonly float _maxDistance = 50f;

    private void Awake()
    {
        _camera = Camera.main.transform;
        _sizeVector = Vector3.one * _pointSize;
    }

    private void Update()
    {
        Ray ray = new Ray(_ray.position, _ray.forward);

        if (Physics.Raycast(ray, out RaycastHit hit, _maxDistance, _layerMask, QueryTriggerInteraction.Ignore))
        {
            _ray.localScale = new Vector3(1, 1, hit.distance);
            _point.position = hit.point;
            float distance = Vector3.Distance(_camera.position, hit.point);
            _point.localScale = _sizeVector * Mathf.Log(distance);
        }
    }
}