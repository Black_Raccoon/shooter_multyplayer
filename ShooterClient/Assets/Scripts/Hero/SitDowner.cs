using System;
using Common;
using UnityEngine;

namespace Hero
{
    public class SitDowner : MonoBehaviour
    {
        public bool IsSitting { get; private set; } = false;

        [SerializeField] private Collider _collider;
        [SerializeField] private CheckFly _checkFly;
        [SerializeField] private float _deltaH;
        [SerializeField] private Transform _head;
        [SerializeField] private Transform _body;

        private Bounds _bounds;
        private Vector3 _originalColliderSize;
        private Vector3 _originalColliderCenter;
        private Vector3 _originalHeadPosition;
        private Vector3 _originalBodyScale;
        private Vector3 _sitDownColliderSize;
        private Vector3 _sitDownColliderCenter;
        private Vector3 _sitDownBodyScale;
        private Vector3 _sitDownHeadPosition;


        private void Awake()
        {
            _bounds = _collider.bounds;

            GetOriginalSize();
            GetSitDownSize();
        }

        public void SitDown()
        {
            CompressCollider();
            CompressBody();
            MoveHeadDown();

            IsSitting = true;
        }

        public void StandUp()
        {
            StretchCollider();
            StretchBody();
            MoveHeadUp();

            IsSitting = false;
        }

        public void SetState(bool sitDown)
        {
            if (sitDown)
                SitDown();
            else
                StandUp();
        }

        private void GetOriginalSize()
        {
            _originalColliderSize = _bounds.size;
            _originalColliderCenter = _bounds.center;
            _originalBodyScale = _body.localScale;
            _originalHeadPosition = _head.localPosition;
        }

        private void GetSitDownSize()
        {
            _sitDownColliderSize = _originalColliderSize;
            _sitDownColliderSize.y -= _deltaH;

            _sitDownColliderCenter = _originalColliderCenter;
            _sitDownColliderCenter.y -= _deltaH;

            _sitDownBodyScale = _originalBodyScale;
            _sitDownBodyScale.z -= _deltaH;

            _sitDownHeadPosition = _originalHeadPosition;
            _sitDownHeadPosition.z -= _deltaH;
        }

        private void CompressCollider()
        {
            _bounds.size = _sitDownColliderSize;
            _bounds.center = _sitDownColliderCenter;
        }

        private void StretchCollider()
        {
            _bounds.size = _originalColliderSize;
            _bounds.center = _originalColliderSize;
        }

        private void CompressBody() =>
            _body.localScale = _sitDownBodyScale;

        private void StretchBody() =>
            _body.localScale = _originalBodyScale;

        private void MoveHeadDown() =>
            _head.localPosition = _sitDownHeadPosition;

        private void MoveHeadUp() =>
            _head.localPosition = _originalHeadPosition;
    }
}