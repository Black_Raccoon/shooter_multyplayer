using UnityEngine;

namespace Hero
{
    public class SpawnPoints : MonoBehaviour
    {
        public int Lenght => _points.Length;
        [SerializeField] private Transform[] _points;

        public Vector3 GetPosition(int index)
        {
            return index >= Lenght ? default : _points[index].position;
        }

        public Vector3 GetEulerAngles(int index)
        {
            return index >= Lenght ? default : _points[index].eulerAngles;
        }

        // public SpawnInfo GetPoints()
        // {
        //     Vector3[] points = new Vector3[_points.Length];
        //     for (var index = 0; index < _points.Length; index++)
        //     {
        //         points[index] = _points[index].position;
        //     }
        //
        //     return new SpawnInfo(points: points);
        // }

        // public struct SpawnInfo
        // {
        //     public Vector3[] Points;
        //
        //     public SpawnInfo(Vector3[] points)
        //     {
        //         Points = points;
        //     }
        // }
    }
}