using Hero.Weapon;
using UnityEngine;

namespace Hero.Player
{
    public class PlayerGun : Gun
    {
        [SerializeField] private Transform _spawnPoint;
        [SerializeField] private float _bulletSpeed;
        [SerializeField] private float _delay;
        [SerializeField] private int _damage;
        private float _prevShootTime;

        public bool TryShoot(out ShootInfo info)
        {
            info = new ShootInfo();

            if (Time.time - _prevShootTime < _delay)
                return false;
            _prevShootTime = Time.time;

            Vector3 position = _spawnPoint.position;
            Vector3 velocity = _spawnPoint.forward * _bulletSpeed;

            Shot(position, velocity, _damage);

            info.pX = position.x;
            info.pY = position.y;
            info.pZ = position.z;
            info.dX = velocity.x;
            info.dY = velocity.y;
            info.dZ = velocity.z;
            return true;
        }
    }
}