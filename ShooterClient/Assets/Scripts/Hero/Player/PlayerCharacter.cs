using System.Collections.Generic;
using Colyseus.Schema;
using UnityEngine;

namespace Hero.Player
{
    public class PlayerCharacter : Character
    {
        [SerializeField] private Health _health;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Transform _head;
        [SerializeField] private Transform _cameraPoint;
        [SerializeField] private CheckFly _checkFly;
        [SerializeField] private float _minHeadAngle = -90;
        [SerializeField] private float _maxHeadAngle = 90;
        [SerializeField] private float _jumpForce = 50f;
        [SerializeField] private float _jumpDelay = 0.2f;

        private float _currentRotateX;
        private float _inputH;
        private float _inputV;
        private float _rotateY;
        private float _jumpTime;
        private bool _isSiting;

        public void SetInput(float h, float v, float rotateY)
        {
            _rotateY += rotateY;
            _inputH = h;
            _inputV = v;
        }

        public void RotateX(float value)
        {
            _currentRotateX = Mathf.Clamp(_currentRotateX + value, _minHeadAngle, _maxHeadAngle);
            _head.localEulerAngles = new Vector3(_currentRotateX, 0, 0);
        }

        public void Jump()
        {
            if (_checkFly.IsFly)
                return;
            if (Time.time - _jumpTime < _jumpDelay)
                return;

            Debug.Log($"Class: PlayerCharacter, methods: Jump is invoked");
            _jumpTime = Time.time;
            _rigidbody.AddForce(0, _jumpForce, 0, ForceMode.VelocityChange);
        }

        public void SetCurrentHp(int hp)
        {
            _health.SetCurrent(hp);
        }

        private void Start()
        {
            Transform camera = Camera.main.transform;
            camera.parent = _cameraPoint;
            camera.localPosition = Vector3.zero;
            camera.localRotation = Quaternion.identity;

            _health.SetMax(MaxHealth);
            _health.SetCurrent(MaxHealth);
        }

        private void FixedUpdate()
        {
            Move();
            RotateY();
        }

        public void GetMoveInfo(out Vector3 position, out Vector3 velocity, out float rotateX, out float rotateY,
            out float angularVelocityY)
        {
            position = transform.position;
            velocity = _rigidbody.velocity;

            rotateX = _head.localEulerAngles.x;
            rotateY = transform.eulerAngles.y;

            angularVelocityY = _rigidbody.angularVelocity.y;
        }

        private void Move()
        {
            Vector3 velocity = (transform.forward * _inputV + transform.right * _inputH).normalized * Speed;
            velocity.y = _rigidbody.velocity.y;
            _rigidbody.velocity = velocity;
            Velocity = velocity;
        }

        private void RotateY()
        {
            _rigidbody.angularVelocity = new Vector3(0, _rotateY, 0);
            _rotateY = 0;
        }
    }
}