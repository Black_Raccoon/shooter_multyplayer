using System;
using System.Collections;
using System.Collections.Generic;
using Colyseus.Schema;
using Hero.Weapon;
using Multiplayer;
using UnityEngine;

namespace Hero.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerCharacter _playerCharacter;
        [SerializeField] private float _restartDelay;
        [SerializeField] private PlayerWeapon _playerWeapon;
        [SerializeField] private float _mouseSensitivity = 2f;
        [SerializeField] private SitDowner _sitDowner;
        private MultiplayerManager _multiplayerManager;
        private Multiplayer.Generated.Player _player;
        private bool _hold;
        private static bool _windowIsFocused;


        private void Start()
        {
            _multiplayerManager = MultiplayerManager.Instance;
            _windowIsFocused = true;
            UpdateCursorState();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _windowIsFocused = !_windowIsFocused;
                UpdateCursorState();
            }

            if (_hold)
                return;

            if (_windowIsFocused)
            {
                MoveProcess();
                JumpProcess();
                ShootProcess();
                SitDownProcess();
                ChangeGunProcess();
            }

            SendMove();
        }

        private void ChangeGunProcess()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                _playerWeapon.SetGun(0);
                SendChangeGun();
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                _playerWeapon.SetGun(1);
                SendChangeGun();
            }
        }

        private void SitDownProcess()
        {
            bool sitDown = Input.GetKey(KeyCode.F);
            if (sitDown)
                _sitDowner.SitDown();
            else
                _sitDowner.StandUp();
        }

        private void ShootProcess()
        {
            bool isShoot = Input.GetMouseButton(0);
            if (isShoot && _playerWeapon.Attack(out ShootInfo shootInfo))
            {
                SendShoot(ref shootInfo);
            }
        }

        private void MoveProcess()
        {
            float h = Input.GetAxisRaw("Horizontal");
            float v = Input.GetAxisRaw("Vertical");
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");
            _playerCharacter.SetInput(h, v, mouseX * _mouseSensitivity);
            _playerCharacter.RotateX(-mouseY * _mouseSensitivity);
        }

        private void JumpProcess()
        {
            bool space = Input.GetKeyDown(KeyCode.Space);
            if (space)
            {

                _playerCharacter.Jump();
            }
        }

        private void SendChangeGun()
        {
            Dictionary<string, object> gunData = new Dictionary<string, object>();
            gunData.Add("index", _playerWeapon.CurrentGunIndex);
            _multiplayerManager.SendMessage("changeGun", gunData);
        }

        public void Init(Multiplayer.Generated.Player player)
        {
            _player = player;
            player.OnChange += OnChange;
        }

        private void OnChange(List<DataChange> changes)
        {
            foreach (DataChange dataChange in changes)
            {
                switch (dataChange.Field)
                {
                    case "currentHp":
                        _playerCharacter.SetCurrentHp((sbyte)dataChange.Value);
                        break;
                    case "loss":
                        MultiplayerManager.Instance.LossCounter.SetPlayerLoss((sbyte)dataChange.Value);
                        break;
                    default:
                        Debug.LogWarning("Unknow field changed" + dataChange.Field);
                        break;
                }
            }
        }

        public void RestartAt(Vector3 position, Vector3 eulerAngles)
        {
            StartCoroutine(RestartHoldDelay());

            _playerCharacter.transform.position = position; //new Vector3(restartInfo.x, 0, restartInfo.z);
            eulerAngles.x = 0;
            eulerAngles.z = 0;
            _playerCharacter.transform.eulerAngles = eulerAngles;
            _playerCharacter.SetInput(0, 0, 0);

            Dictionary<string, object> data = new Dictionary<string, object>
            {
                { "pX", position.x },
                { "pY", position.y },
                { "pZ", position.z },
                { "vX", 0 },
                { "vY", 0 },
                { "vZ", 0 },
                { "rX", 0 },
                { "rY", eulerAngles.y },
                { "avY", 0 },
                { "sD", 0 }
            };
            _multiplayerManager.SendMessage("move", data);
        }

        private IEnumerator RestartHoldDelay()
        {
            _hold = true;
            yield return new WaitForSecondsRealtime(_restartDelay);
            _hold = false;
        }

        private void SendShoot(ref ShootInfo shootInfo)
        {
            shootInfo.key = _multiplayerManager.GetSessionId();
            string shootData = JsonUtility.ToJson(shootInfo);
            _multiplayerManager.SendMessage("shoot", shootData);
        }

        private void SendMove()
        {
            _playerCharacter.GetMoveInfo(out Vector3 position, out Vector3 velocity, out float rotateX,
                out float rotateY, out float angularVelocityY);

            Dictionary<string, object> data = new Dictionary<string, object>
            {
                { "pX", position.x },
                { "pY", position.y },
                { "pZ", position.z },
                { "vX", velocity.x },
                { "vY", velocity.y },
                { "vZ", velocity.z },
                { "rX", rotateX },
                { "rY", rotateY },
                { "avY", angularVelocityY },
                { "sD", _sitDowner.IsSitting }
            };
            _multiplayerManager.SendMessage("move", data);
        }

        private static void UpdateCursorState()
        {
            Cursor.lockState = _windowIsFocused switch
            {
                true => CursorLockMode.Locked,
                false => CursorLockMode.None
            };
        }
    }

    [Serializable]
    public struct ShootInfo
    {
        public string key;
        public float pX;
        public float pY;
        public float pZ;
        public float dX;
        public float dY;
        public float dZ;
    }

    [Serializable]
    public struct RestartInfo
    {
        public float x;
        public float z;
    }
}