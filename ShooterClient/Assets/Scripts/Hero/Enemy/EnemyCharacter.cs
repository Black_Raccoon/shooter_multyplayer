using System;
using Unity.Collections;
using UnityEngine;

namespace Hero.Enemy
{
    public class EnemyCharacter : Character
    {
        public event Action<int> OnTakeDamage;


        [SerializeField] private Transform _head;
        private float _velocityMagnitude = 0;
        private Vector3 _targetHeadRotation;

        private float _targetYRotation;
        private float _angularVelocityY;

        private float _targetXRotation;
        private float _angularVelocityX;
        [SerializeField] private Health _health;

        public Vector3 TargetPosition { get; private set; } = Vector3.zero;

        private void Start()
        {
            TargetPosition = transform.position;
            _targetYRotation = transform.eulerAngles.y;
            _health.OnTakeDamage += OnTakeDamages;
        }

        private void Update()
        {
            UpdateMovement();
            UpdateXRotation();
            UpdateYRotation();
        }

        private void OnDestroy()
        {
            _health.OnTakeDamage -= OnTakeDamages;
        }

        public void SetXRotation(float xRotation, float prevXRotation, float averageInterval)
        {
            _angularVelocityX = (xRotation - prevXRotation) / averageInterval / 180 * Mathf.PI;
            _targetXRotation = xRotation + (xRotation - prevXRotation);
        }

        public void SetYRotation(float yRotation, float angularVelocityY, float averageInterval)
        {
            _targetYRotation = yRotation + angularVelocityY * 180 / Mathf.PI * averageInterval;
            _angularVelocityY = angularVelocityY;
        }

        public void SetMovement(in Vector3 position, in Vector3 velocity, in float averageInterval)
        {
            TargetPosition = position + velocity * averageInterval;
            Velocity = velocity;
            _velocityMagnitude = velocity.magnitude;
        }

        public void GetMoveInfo(out Vector3 targetPosition, out Vector3 velocity)
        {
            targetPosition = TargetPosition;
            velocity = Velocity;
        }

        public void GetYRotationInfo(out float yRotation, out float angularVelocityY)
        {
            angularVelocityY = _angularVelocityY;
            yRotation = _targetYRotation;
        }

        public void GetXRotationInfo(out float xRotation) =>
            xRotation = _targetXRotation;

        public void ApplyDamage(int damage)
        {
            _health.ApplyDamage(damage);
        }

        public void SetSpeed(float value) =>
            Speed = value;

        public void SetMaxHP(int value)
        {
            MaxHealth = value;
            _health.SetMax(value);
            _health.SetCurrent(value);
        }

        private void OnTakeDamages(int currentHp)
        {
            OnTakeDamage?.Invoke(currentHp);
        }

        private void UpdateMovement()
        {
            if (_velocityMagnitude > 0.1f)
                transform.position = Vector3.MoveTowards(transform.position, TargetPosition,
                    _velocityMagnitude * Time.deltaTime);
            else
                transform.position = TargetPosition;
        }

        private void UpdateXRotation()
        {
            float angle;
            if (Mathf.Abs(_angularVelocityX) > 0.1f)
                angle = MoveTowardsAngle(_head.localEulerAngles.x, _targetXRotation, _angularVelocityX);
            else
                angle = _targetXRotation;

            _head.localRotation = Quaternion.Euler(angle, 0, 0);
        }

        private void UpdateYRotation()
        {
            float angle;
            if (Mathf.Abs(_angularVelocityY) > 0.01f)
                angle = MoveTowardsAngle(transform.localEulerAngles.y, _targetYRotation, _angularVelocityY);
            else
                angle = _targetYRotation;

            transform.localRotation = Quaternion.Euler(0, angle, 0);
        }

        private static float MoveTowardsAngle(float localEulerAnglesY, float targetYRotation, float angularVelocity)
        {
            return Mathf.MoveTowardsAngle(localEulerAnglesY, targetYRotation,
                Mathf.Abs(angularVelocity) * 180 / Mathf.PI * Time.deltaTime);
        }

        public void SetHealth(int newValue)
        {
            _health.SetCurrent(newValue);
        }
    }
}