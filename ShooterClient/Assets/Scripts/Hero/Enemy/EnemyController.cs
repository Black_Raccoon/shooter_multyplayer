using System;
using System.Collections.Generic;
using Colyseus.Schema;
using Common;
using Hero.Player;
using Hero.Weapon;
using Multiplayer;
using UnityEngine;

namespace Hero.Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemyCharacter _enemyCharacter;
        [SerializeField] private SitDowner _sitDowner;
        [SerializeField] private EnemyWeapon _enemyWeapon;
        [SerializeField] private int _gun;

        private readonly IntervalCalculator _intervalCalculator = new();
        private Multiplayer.Generated.Player _player;
        private MultiplayerManager _multiplayerManager;
        private string _key;

        private void Start()
        {
            _multiplayerManager = MultiplayerManager.Instance;
        }

        public void Init(string key, Multiplayer.Generated.Player player)
        {
            _key = key;
            _player = player;
            _enemyCharacter.SetSpeed(player.speed);
            _enemyCharacter.SetMaxHP(player.maxHp);
            _player.OnChange += OnChange;
            _enemyCharacter.OnTakeDamage += OnTakeDamage;
        }

        public void Shoot(in ShootInfo shootInfo)
        {
            Vector3 position = new Vector3(shootInfo.pX, shootInfo.pY, shootInfo.pZ);
            Vector3 velocity = new Vector3(shootInfo.dX, shootInfo.dY, shootInfo.dZ);

            _enemyWeapon.Attack(position, velocity);
        }

        public void Destroy()
        {
            _player.OnChange -= OnChange;
            _enemyCharacter.OnTakeDamage -= OnTakeDamage;
            Destroy(gameObject);
        }

        public void OnChange(List<DataChange> dataChanges)
        {
            _intervalCalculator.SaveTime();
            _enemyCharacter.GetMoveInfo(out Vector3 targetPosition, out Vector3 velocity);
            _enemyCharacter.GetXRotationInfo(out float xRotation);
            _enemyCharacter.GetYRotationInfo(out float yRotation, out float angularVelocityY);
            float prevXRotation = xRotation;
            foreach (DataChange dataChange in dataChanges)
            {
                switch (dataChange.Field)
                {
                    case "gun":
                        _enemyWeapon.SetGun((sbyte)dataChange.Value);
                        break;
                    case "loss":
                        MultiplayerManager.Instance.LossCounter.SetEnemyLoss((sbyte)dataChange.Value);
                        break;
                    case "currentHp":
                        if ((sbyte)dataChange.Value > (sbyte)dataChange.PreviousValue)
                            _enemyCharacter.SetHealth((sbyte)dataChange.Value);
                        break;
                    case "pX":
                        targetPosition.x = (float)dataChange.Value;
                        break;
                    case "pY":
                        targetPosition.y = (float)dataChange.Value;
                        break;
                    case "pZ":
                        targetPosition.z = (float)dataChange.Value;
                        break;
                    case "vX":
                        velocity.x = (float)dataChange.Value;
                        break;
                    case "vY":
                        velocity.y = (float)dataChange.Value;
                        break;
                    case "vZ":
                        velocity.z = (float)dataChange.Value;
                        break;
                    case "rX":
                        xRotation = (float)dataChange.Value;
                        prevXRotation = (float)dataChange.PreviousValue;
                        break;
                    case "rY":
                        yRotation = (float)dataChange.Value;
                        break;
                    case "avY":
                        angularVelocityY = (float)dataChange.Value;
                        break;
                    case "sD":
                        _sitDowner.SetState((bool)dataChange.Value);
                        break;
                    default:
                  //      Debug.LogWarning("Unknow field changed" + dataChange.Field);
                        break;
                }
            }

            _enemyCharacter.SetMovement(targetPosition, velocity, _intervalCalculator.AverageInterval);
            _enemyCharacter.SetYRotation(yRotation, angularVelocityY, _intervalCalculator.AverageInterval);
            _enemyCharacter.SetXRotation(xRotation, prevXRotation, _intervalCalculator.AverageInterval);
        }

        private void OnTakeDamage(int currentHp)
        {
            Dictionary<string, object> data = new Dictionary<string, object>()
            {
                { "id", _key },
                { "currentHp", currentHp },
            };
            _multiplayerManager.SendMessage("damage", data);
        }
    }
}