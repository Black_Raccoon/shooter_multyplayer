using UnityEngine;

namespace Hero.Weapon
{
    public class GunAnimation : MonoBehaviour
    {
        [SerializeField] private Gun _playerGun;
        [SerializeField] private Animator _shootAnimator;
        private static readonly int Shoot = Animator.StringToHash("Shoot");

        private void Start()
        {
            _playerGun.OnShoot += OnShot;
        }

        private void OnShot()
        {
            _shootAnimator.SetTrigger(Shoot);
        }

        private void OnDestroy()
        {
            _playerGun.OnShoot -= OnShot;
        }
    }
}