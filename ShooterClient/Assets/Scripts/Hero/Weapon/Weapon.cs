using UnityEngine;

namespace Hero.Weapon
{
    public abstract class Weapon : MonoBehaviour
    {
        [SerializeField] protected Gun[] _guns;
        public int CurrentGunIndex { get; private set; }

        private void Start()
        {
            SetGun(CurrentGunIndex);
        }


        public void SetGun(int gunIndex)
        {
            CurrentGunIndex = gunIndex;
            for (int i = 0; i < _guns.Length; i++)
            {
                if (i == gunIndex)
                    _guns[i].Activate();
                else
                    _guns[i].Deactivate();
            }
        }
    }
}