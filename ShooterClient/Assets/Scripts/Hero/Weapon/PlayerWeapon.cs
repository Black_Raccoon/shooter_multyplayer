using Hero.Player;

namespace Hero.Weapon
{
    public class PlayerWeapon : Weapon
    {
        public bool Attack(out ShootInfo info)
        {
            PlayerGun playerGun = (PlayerGun)_guns[CurrentGunIndex];
            return playerGun.TryShoot(out info);
        }
    }
}