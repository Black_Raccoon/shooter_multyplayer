using UnityEngine;

namespace Hero.Weapon
{
    class EnemyWeapon : Weapon
    {
        public void Attack(Vector3 position, Vector3 velocity)
        {
            _guns[CurrentGunIndex].Shot(position, velocity);
        }
    }
}