using System;
using UnityEngine;

namespace Hero.Weapon
{
    public class Gun : MonoBehaviour
    {
        public Action OnShoot;
        [SerializeField] protected Bullet _bulletPrefab;

        public void Shot(Vector3 position, Vector3 velocity, int damage = 0)
        {
            Bullet bullet = Instantiate(_bulletPrefab, position, Quaternion.identity);
            bullet.Init(velocity, damage);
            OnShoot?.Invoke();
        }
        
        public void Activate()
        {
            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }
    }
}