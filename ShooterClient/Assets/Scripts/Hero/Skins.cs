using UnityEngine;

namespace Hero
{
    public class Skins : MonoBehaviour
    {
        public int Length => _materials.Length;
        public Material this[int index] => index > _materials.Length ? _materials[0] : _materials[index];
        
        [SerializeField] private Material[] _materials;
    }
}