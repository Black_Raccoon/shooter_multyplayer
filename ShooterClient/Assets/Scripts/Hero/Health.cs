using System;
using UI;
using UnityEngine;

namespace Hero
{
    public class Health : MonoBehaviour
    {
        public event Action<int, int> OnHealthChanged;
        public event Action<int> OnTakeDamage;

        private int _max;
        private int _current;

        public void SetMax(int max)
        {
            _max = max;
            OnHealthChanged?.Invoke(_max, _current);
        }

        public void SetCurrent(int current)
        {
            _current = current;
            OnHealthChanged?.Invoke(_max, _current);
        }

        public void ApplyDamage(int damage)
        {
            _current -= damage;
            OnHealthChanged?.Invoke(_max, _current);
            OnTakeDamage?.Invoke(_current);
        }
    }
}