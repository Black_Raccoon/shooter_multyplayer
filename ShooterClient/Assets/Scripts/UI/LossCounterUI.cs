using Common;
using TMPro;
using UnityEngine;

namespace UI
{
    public class LossCounterUI : MonoBehaviour
    {
        [SerializeField] private LossCounter _lossCounter;
        [SerializeField] private TMP_Text _text;

        private void Start()
        {
            _lossCounter.OnCountChanged += Show;
        }

        public void Show(int enemyLoss, int playerLoss)
        {
            _text.text = $"{playerLoss}:{enemyLoss}";
        }
    }
}