using System;
using UnityEngine;

namespace UI
{
    public class LookAtCamera : MonoBehaviour
    {
        private Transform _target;

        private void Start()
        {
            _target = Camera.main.transform;
        }

        private void Update()
        {
            transform.LookAt(_target);
        }
    }
}