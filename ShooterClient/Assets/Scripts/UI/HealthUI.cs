using System;
using Hero;
using UnityEngine;

namespace UI
{
    public class HealthUI : MonoBehaviour
    {
        [SerializeField] private Health _health;

        [SerializeField] private RectTransform _filledImage;
        private float _defaultWidth;

        private void Start()
        {
            _defaultWidth = _filledImage.sizeDelta.x;
            _health.OnHealthChanged += Show;
        }

        public void Show(int max, int current)
        {
            float filled = current / (float)max;
            _filledImage.sizeDelta = new Vector2(filled * _defaultWidth, _filledImage.sizeDelta.y);
        }
    }
}