using System.Collections.Generic;
using Colyseus;
using Common;
using Hero;
using Hero.Enemy;
using Hero.Player;
using Multiplayer.Generated;
using UnityEngine;

namespace Multiplayer
{
    public class MultiplayerManager : ColyseusManager<MultiplayerManager>
    {
        [SerializeField] private Factory _factory;
        [SerializeField] private SpawnPoints _spawnPoints;

        [field: SerializeField] public LossCounter LossCounter { get; private set; }

        private ColyseusRoom<State> _room;
        private Dictionary<string, EnemyController> _enemies;
        private PlayerController _playerController;

        protected override void Awake()
        {
            base.Awake();
            Instance.InitializeClient();
            Connect();

            //  _enemies = _factory.Enemies;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _room.Leave();
        }

        public string GetSessionId() =>
            _room.SessionId;

        public void SendMessage(string key, Dictionary<string, object> data) =>
            _room.Send(key, data);

        public void SendMessage(string key, string data) =>
            _room.Send(key, data);

        private async void Connect()
        {
            _enemies = _factory.Enemies;

            Dictionary<string, object> data = new Dictionary<string, object>
            {
                {"skinsCount", _factory.Skins.Length},
                { "speed", _factory.PlayerSpeed },
                { "hp", _factory.PlayerHp },
                { "countPoint", _spawnPoints.Lenght },
             //   { "spawnPoint", _spawnPoints.GetPoints() }
            };

            _room = await Instance.client.JoinOrCreate<State>("state_handler", data);
            _room.OnStateChange += OnChange;
            _room.OnMessage<string>("SRV_shoot", ApplyShot);
        }

        private void ApplyShot(string jsonShootInfo)
        {
            ShootInfo shootInfo = JsonUtility.FromJson<ShootInfo>(jsonShootInfo);

            if (!_enemies.ContainsKey(shootInfo.key))
            {
                Debug.LogError("The enemy is missing, but he tried to shoot");
                return;
            }

            _enemies[shootInfo.key].Shoot(shootInfo);
        }

        private void OnChange(State state, bool isFirstState)
        {
            if (!isFirstState)
                return;

            state.players.ForEach((key, player) =>
            {
                if (key == _room.SessionId)
                {
                    _playerController = _factory.CreatePlayerAt(player, _spawnPoints.GetPosition((int)player.pX), _spawnPoints.GetEulerAngles((int)player.pX));
                    _room.OnMessage<int>("Restart", Restart); //_factory.PlayerController.Restart);
                }
                else
                    _factory.CreateEnemy(key, player);
            });

            _room.State.players.OnAdd += _factory.CreateEnemy;
            _room.State.players.OnRemove += _factory.RemoveEnemy;
        }

        private void Restart(int index)
        {
            _playerController.RestartAt(_spawnPoints.GetPosition(index), _spawnPoints.GetEulerAngles(index));
        }
    }
}