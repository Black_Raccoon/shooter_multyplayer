using System.Collections.Generic;
using Hero;
using Hero.Enemy;
using Hero.Player;
using Multiplayer.Generated;
using UnityEngine;

namespace Multiplayer
{
    public class Factory : MonoBehaviour
    {
        public PlayerController PlayerController { get; private set; }
        public Skins Skins => _skins;
        public float PlayerSpeed => _playerPrefab.Speed;
        public float PlayerHp => _playerPrefab.MaxHealth;
        public Dictionary<string, EnemyController> Enemies => _enemies;


        [SerializeField] private Skins _skins;
        [SerializeField] private PlayerCharacter _playerPrefab;
        [SerializeField] private EnemyController _enemyPrefab;

        private Dictionary<string, EnemyController> _enemies = new();

        public PlayerController CreatePlayerAt(Player player, Vector3 position, Vector3 eulerAngles)
        {
            //Vector3 position = new Vector3(player.pX, player.pY, player.pZ);
            PlayerCharacter playerCharacter = Instantiate(_playerPrefab, position, Quaternion.Euler(eulerAngles));
            
            if(playerCharacter.TryGetComponent(out SkinSetter skinSetter))
                skinSetter.Set(Skins[player.skin]);
            
            PlayerController = playerCharacter.GetComponent<PlayerController>();
            PlayerController.Init(player);
            return PlayerController;
        }

        public void CreateEnemy(string key, Player player)
        {
            Vector3 position = new Vector3(player.pX, player.pY, player.pZ);
            EnemyController enemy = Instantiate(_enemyPrefab, position, Quaternion.Euler(0, player.rY, 0));
            
            if(enemy.TryGetComponent(out SkinSetter skinSetter))
                skinSetter.Set(Skins[player.skin]);
            
            enemy.Init(key, player);
            Enemies.Add(key, enemy);
            player.OnChange += enemy.OnChange;
        }

        public void RemoveEnemy(string key, Player player)
        {
            if (!Enemies.ContainsKey(key))
                return;

            Enemies[key].Destroy();
            Enemies.Remove(key);
        }
    }
}